#SingleInstance, Force
#KeyHistory, 0
SetBatchLines, -1
ListLines, Off
SendMode Input ; Forces Send and SendRaw to use SendInput buffering for speed.
SetTitleMatchMode, 3 ; A window's title must exactly match WinTitle to be a match.
SetWorkingDir, %A_ScriptDir%
SplitPath, A_ScriptName, , , , thisscriptname
#MaxThreadsPerHotkey, 1 ; no re-entrant hotkey handling
; DetectHiddenWindows, On
; SetWinDelay, -1 ; Remove short delay done automatically after every windowing command except IfWinActive and IfWinExist
; SetKeyDelay, -1, -1 ; Remove short delay done automatically after every keystroke sent by Send or ControlSend
; SetMouseDelay, -1 ; Remove short delay done automatically after Click and MouseMove/Click/Drag

ToolTipWait()
global keysPressed := ""

ClipboardOnetimeWrite(textToWrite){ 
    ClipSaved := ClipboardAll   ; Save the entire clipboard to a variable of your choice.
    clipboard := textToWrite
    SendInput,^v 
    Sleep 100 ; Sleep because otherwise the clipboard will be overriden before windows can react
    clipboard := ClipSaved   ; Restore the original clipboard. Note the use of Clipboard (not ClipboardAll).
    ClipSaved := ""   ; Free the memory in case the clipboard was very large.
}

ClipboardWrite(textToWrite){ 
    clipboard := textToWrite
    SendInput,^v
}
 

ActivateCommands(){ 
    if (keysPressed == "k"){
        Send {U+2713} ;✓
    }
    else if (keysPressed == "date"){ 
        FormatTime, CurrentDateTime,, yyyy-MM-dd
        ClipboardWrite(CurrentDateTime)
    }
    else if (keysPressed == "guid"){
        TypeLib := ComObjCreate("Scriptlet.TypeLib")
        NewGUID := TypeLib.Guid
        NewGUID := StrReplace(NewGUID, "{", "")
        NewGUID := StrReplace(NewGUID, "}", "")
        ClipboardWrite(NewGUID)
    }
    else{
        return False
    }
    return True
}

CommandKey(key){
    if (key == "BACKSPACE"){
        keysPressed := SubStr(keysPressed,1,StrLen(keysPressed)-1) 
        if (!keysPressed){ 
            ToolTipWait()
            return
        }
    }
    else{
        keysPressed := keysPressed . key
    }   
    ToolTip, % keysPressed, 0, 0
    Suspend,On
    if (ActivateCommands()){
        Abort()
    }
    else{
        Suspend,Off
    }
}

Abort(){
    keysPressed := "" 
    ToolTip, 
    Suspend,On ; Toggle on and off on hotkeys
}

ToolTipWait(){
    ToolTip, Waiting for command..., 0, 0 
}
 
a::CommandKey("a")
b::CommandKey("b") 
c::CommandKey("c")   
d::CommandKey("d")
e::CommandKey("e")
f::CommandKey("f")
g::CommandKey("g")
h::CommandKey("h")
i::CommandKey("i")
j::CommandKey("j")
k::CommandKey("k")
l::CommandKey("l")
m::CommandKey("m")
n::CommandKey("n")
o::CommandKey("o")
p::CommandKey("p")
q::CommandKey("q")
r::CommandKey("r")
s::CommandKey("s")
t::CommandKey("t")
u::CommandKey("u")
v::CommandKey("v")
w::CommandKey("w")
x::CommandKey("x")
y::CommandKey("y")
z::CommandKey("z")
0::CommandKey("0")
1::CommandKey("1")
2::CommandKey("2")
3::CommandKey("3")
4::CommandKey("4")
5::CommandKey("5")
6::CommandKey("6")
7::CommandKey("7")
8::CommandKey("8")
9::CommandKey("9")
`::CommandKey("`")
~::CommandKey("~")
!::CommandKey("!")
@::CommandKey("@")
#::CommandKey("#")
$::CommandKey("$")
%::CommandKey("%")
^::CommandKey("^")
&::CommandKey("&")
*::CommandKey("*")
(::CommandKey("(")
)::CommandKey(")")
-::CommandKey("-")
_::CommandKey("_")
=::CommandKey("=")
+::CommandKey("+")
[::CommandKey("[")
{::CommandKey("{")
]::CommandKey("]")
}::CommandKey("}")
\::CommandKey("\")
|::CommandKey("|")
`;::CommandKey(";")
'::CommandKey("'")
,::CommandKey(",")
<::CommandKey("<")
.::CommandKey(".")
>::CommandKey(">")
Backspace::CommandKey("BACKSPACE")
Enter::Abort()
Esc:: Abort()
; Capslock & Space::
;     Suspend,Off
;     ToolTipWait()
; return

