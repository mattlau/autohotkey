#SingleInstance, Force
#KeyHistory, 0
SetBatchLines, -1
ListLines, Off
SendMode Input ; Forces Send and SendRaw to use SendInput buffering for speed.
SetTitleMatchMode, 3 ; A window's title must exactly match WinTitle to be a match.
SetWorkingDir, %A_ScriptDir%
SplitPath, A_ScriptName, , , , thisscriptname
#MaxThreadsPerHotkey, 1 ; no re-entrant hotkey handling
#MaxHotkeysPerInterval, 300
; DetectHiddenWindows, On
; SetWinDelay, -1 ; Remove short delay done automatically after every windowing command except IfWinActive and IfWinExist
; SetKeyDelay, -1, -1 ; Remove short delay done automatically after every keystroke sent by Send or ControlSend
; SetMouseDelay, -1 ; Remove short delay done automatically after Click and MouseMove/Click/Dragæ

#SingleInstance, Force
#KeyHistory, 0
SetBatchLines, -1
ListLines, Off
SendMode Input ; Forces Send and SendRaw to use SendInput buffering for speed.
SetTitleMatchMode, 3 ; A window's title must exactly match WinTitle to be a match.
SetWorkingDir, %A_ScriptDir%
SplitPath, A_ScriptName, , , , thisscriptname
#MaxThreadsPerHotkey, 1 ; no re-entrant hotkey handling
; DetectHiddenWindows, On
; SetWinDelay, -1 ; Remove short delay done automatically after every windowing command except IfWinActive and IfWinExist
; SetKeyDelay, -1, -1 ; Remove short delay done automatically after every keystroke sent by Send or ControlSend
; SetMouseDelay, -1 ; Remove short delay done automatically after Click and MouseMove/Click/Drag

global pressedOtherKeys := false
global inSpaceMode := false

EnableSpaceMode(){
    if (!inSpaceMode){
        ToolTipWait()
        inSpaceMode := true
    }
}
DisableSpaceMode(){
    if (!pressedOtherKeys){      
    }
    Abort()     
}


ClipboardOneTimeWrite(textToWrite){ 
    ClipSaved := ClipboardAll   ; Save the entire clipboard to a variable of your choice.
    clipboard := textToWrite
    SendInput,^v
    Sleep 5 ; Sleep because otherwise the clipboard will be overriden before windows can react
    clipboard := ClipSaved   ; Restore the original clipboard. Note the use of Clipboard (not ClipboardAll).
    ClipSaved := ""   ; Free the memory in case the clipboard was very large.
}

ClipboardWrite(textToWrite){ 
    clipboard := textToWrite
    SendInput,^v
}

keysPressed := ""

CommandKey(key){
    keysPressed := keysPressed . key
    if (GetKeyState("LShift", "P") == 1){
        key := "+" . key
    }
    pressedOtherKeys := True
    if (key == "Space"){
        Run, %A_AHKPath% "%A_ScriptDir%\CommandMode.ahk"
    }
    else if (key == ";"){
        Send {U+00E6} ;
    }
    else if (key == "+;"){ 
        Send {U+00C6} ;Æ
    }
    else if (key == "'"){ ;ø    
        Send {U+00F8}
    }
    else if (key == "+'"){ ;Ø
        Send {U+00D8}
    }
    else if (key == "[" ){ ;å
        Send {U+00E5}
    }
    else if (key == "+["){ ;
        Send {U+00C5}
    }

    ;Movement
    else if (key == "i"){ 
        Send {Up}
    }
    else if (key == "k"){ ;Ø
        Send {Down}
    }
    else if (key == "j" ){ ;å
        Send {Left}
    }
    else if (key == "l"){ ;
        Send {Right}
    }
    else if (key == "u"){ ;
        Send ^{Left}
    }
    else if (key == "o"){ ;
        Send ^{Right}
    }
    else if (key == "w"){
        Send {Up 8}
    }
    else if (key == "s"){
        Send {Down 8}
    }
    else if (key == "a"){
        Send {Home}
    }
    else if (key == "d"){
        Send {End}
    }

    ; Hotkeys
    else if (key == "\"){
        send #\ ; win + \
    }
    else{
        msgtext := Key . " matched nothing"
        Tooltip, %msgtext%, 0, 0
    }
}

Abort(){
    inSpaceMode := false
    pressedOtherKeys := false
    ToolTip, 
    Suspend,On ; Toggle on and off on hotkeys
}

ToolTipWait(){
    ToolTip, Space mode..., 0, 0   
}
 
a::CommandKey("a")
b::CommandKey("b") 
c::CommandKey("c")   
d::CommandKey("d")
e::CommandKey("e")
f::CommandKey("f")
g::CommandKey("g")
h::CommandKey("h")
i::CommandKey("i")
j::CommandKey("j")
k::CommandKey("k")
l::CommandKey("l")
m::CommandKey("m")
n::CommandKey("n")
o::CommandKey("o")
p::CommandKey("p")
q::CommandKey("q")
r::CommandKey("r")
s::CommandKey("s")
t::CommandKey("t")
u::CommandKey("u")
v::CommandKey("v")
w::CommandKey("w")
x::CommandKey("x")
y::CommandKey("y")
z::CommandKey("z")
0::CommandKey("0")
1::CommandKey("1")
2::CommandKey("2")
3::CommandKey("3")
4::CommandKey("4")
5::CommandKey("5")
6::CommandKey("6")
7::CommandKey("7")
8::CommandKey("8")
9::CommandKey("9")
`::CommandKey("`")
!::CommandKey("!")
@::CommandKey("@")
#::CommandKey("#")
$::CommandKey("$")
%::CommandKey("%")
^::CommandKey("^")
&::CommandKey("&")
*::CommandKey("*")   
(::CommandKey("(")
)::CommandKey(")")
-::CommandKey("-")
_::CommandKey("_")
=::CommandKey("=")
+::CommandKey("+")
[::CommandKey("[")
]::CommandKey("]")
\::CommandKey("\")
|::CommandKey("|")
'::CommandKey("'")
`;::CommandKey(";")
,::CommandKey(",")
<::CommandKey("<")
.::CommandKey(".")
>::CommandKey(">")

; Shift modifier is checked for later, so just send the normal key
:::CommandKey(";")
}::CommandKey("]")
{::CommandKey("[")
"::CommandKey("'")
Space::CommandKey("Space")            
; CapsLock & RShift & a::CommandKey("+a")
; +CapsLock & b::CommandKey("+b") 
; +CapsLock & c::CommandKey("+c")   
; +CapsLock & d::CommandKey("+d")
; +CapsLock & e::CommandKey("+e")
; +CapsLock & f::CommandKey("+f")
; +CapsLock & g::CommandKey("+g") 
; +CapsLock & h::CommandKey("+h")
; +CapsLock & i::CommandKey("+i")
; +CapsLock & j::CommandKey("+j")
; +CapsLock & k::CommandKey("+k")
; +CapsLock & l::CommandKey("+l")
; +CapsLock & m::CommandKey("+m")
; +CapsLock & n::CommandKey("+n")
; +CapsLock & o::CommandKey("+o")
; +CapsLock & p::CommandKey("+p")
; +CapsLock & q::CommandKey("+q")
; +CapsLock & r::CommandKey("+r")
; +CapsLock & s::CommandKey("+s")
; +CapsLock & t::CommandKey("+t")
; +CapsLock & u::CommandKey("+u")
; +CapsLock & v::CommandKey("+v")
; +CapsLock & w::CommandKey("+w")
; +CapsLock & x::CommandKey("+x")
; +CapsLock & y::CommandKey("+y")
; +CapsLock & z::CommandKey("+z")
; +CapsLock & [::CommandKey("+[")
; +CapsLock & `;::CommandKey("+;")
; +CapsLock & '::CommandKey("+'")
Escape::DisableSpaceMode()
RAlt & CapsLock::SetCapsLockState, % GetKeyState( "CapsLock", "T" ) ? "OFF" : "ON"
+CapsLock UP::DisableSpaceMode()
CapsLock UP::DisableSpaceMode()
+CapsLock::
    Suspend,Off
    EnableSpaceMode()
CapsLock::
    Suspend,Off
    EnableSpaceMode()
    return

